import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-condicional',
  templateUrl: './condicional.component.html',
  styleUrls: ['./condicional.component.css']
})
export class CondicionalComponent implements OnInit {
  mostrar:boolean
  perfil: any = {
    nombre:"Charly",
    apellido:"Meneces",
    gmail:"ejemplo@gmail.com",
    direccion:"Av. America"
  }
  constructor() {
    this.mostrar=true
   }

  ngOnInit(): void {
  }

}
